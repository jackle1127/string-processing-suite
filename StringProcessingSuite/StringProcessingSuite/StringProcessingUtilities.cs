﻿using System;
using System.Collections.Generic;

namespace Jack.StringProcessingSuite
{
    public class StringProcessingUtilities
    {
        public enum Edit
        {
            Replace = 0,
            Insert,
            Delete,
            None
        }

        public static (int distance, Edit[] editSquence) GetLevenshteinDistance(string a, string b)
        {
            var tempA = "|" + a;
            var tempB = "|" + b;

            int[,] editCostMatrix = new int[tempA.Length, tempB.Length];

            // Set up the matrix
            for (int i = 0; i < tempA.Length; i++)
            {
                editCostMatrix[i, 0] = i;
            }
            for (int i = 0; i < tempB.Length; i++)
            {
                editCostMatrix[0, i] = i;
            }

            for (int y = 1; y < tempB.Length; y++)
            {
                for (int x = 1; x < tempA.Length; x++)
                {
                    int costToReplace = tempA[x] == tempB[y] ? 0 : 1;

                    int replace = editCostMatrix[x - 1, y - 1] + costToReplace;
                    int insert = editCostMatrix[x, y - 1] + 1;
                    int delete = editCostMatrix[x - 1, y] + 1;

                    editCostMatrix[x, y] = Math.Min(replace, Math.Min(insert, delete));
                }
            }

            int xPointer = tempA.Length - 1;
            int yPointer = tempB.Length - 1;
            var editStack = new Stack<Edit>();

            while (xPointer > 0 || yPointer > 0)
            {
                //Console.WriteLine(xPointer + ", " + yPointer);
                int min = int.MaxValue;
                Edit currentEdit = Edit.None;

                if (xPointer > 0 && yPointer > 0 && editCostMatrix[xPointer - 1, yPointer - 1] < min)
                {
                    min = editCostMatrix[xPointer - 1, yPointer - 1];
                    if (editCostMatrix[xPointer - 1, yPointer - 1] == editCostMatrix[xPointer, yPointer])
                    {
                        currentEdit = Edit.None;
                    }
                    else
                    {
                        currentEdit = Edit.Replace;
                    }
                }
                if (xPointer > 0 && editCostMatrix[xPointer - 1, yPointer] < min)
                {
                    min = editCostMatrix[xPointer - 1, yPointer];
                    currentEdit = Edit.Delete;
                }
                if (yPointer > 0 && editCostMatrix[xPointer, yPointer - 1] < min)
                {
                    currentEdit = Edit.Insert;
                }

                switch (currentEdit)
                {
                    case Edit.None:
                    case Edit.Replace:
                        xPointer--;
                        yPointer--;
                        break;
                    case Edit.Insert:
                        yPointer--;
                        break;
                    case Edit.Delete:
                        xPointer--;
                        break;
                }

                editStack.Push(currentEdit);
            }

            int distance = editCostMatrix[tempA.Length - 1, tempB.Length - 1];
            return (distance, editStack.ToArray());
        }
    }
}
