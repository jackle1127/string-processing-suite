﻿using System;
using System.Diagnostics;
using Jack.StringProcessingSuite;

namespace LevenshteinDistanceTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string again = "";
            do
            {
                Console.Write("String 1: ");
                var string1 = Console.ReadLine();
                Console.Write("String 2: ");
                var string2 = Console.ReadLine();

                var stopWatch = Stopwatch.StartNew();
                (var distance, var editSequence) = StringProcessingUtilities.GetLevenshteinDistance(string1, string2);
                stopWatch.Stop();

                var percentSimilarity = 1 - (float)distance / Math.Max(string1.Length, string2.Length);

                Console.WriteLine($"Levenshtein distance: {distance}/{Math.Max(string1.Length, string2.Length)}");
                Console.WriteLine($"Percent similarity: {percentSimilarity * 100}%");
                Console.WriteLine($"Elapsed time: {stopWatch.ElapsedMilliseconds}ms");

                Console.Write($"String 1: ");
                WriteStringEdits(string1, editSequence, true, ConsoleColor.DarkGreen, ConsoleColor.Green);
                Console.WriteLine();
                Console.Write($"String 2: ");
                WriteStringEdits(string2, editSequence, false, ConsoleColor.DarkRed, ConsoleColor.Red);
                Console.WriteLine();

                Console.Write("Again? (y/n)");
                again = Console.ReadLine();
            } while (again != "n");
        }

        static void WriteStringEdits(string compareString, StringProcessingUtilities.Edit[] editSequence, bool isOriginalString, ConsoleColor highlightColor, ConsoleColor spaceHighlightColor)
        {
            int currentCharIndex = 0;
            /*Console.WriteLine();
            foreach (var edit in editSequence)
            {
                Console.WriteLine(edit);
            }*/
            foreach (var edit in editSequence)
            {
                var currentChar = "";
                switch (edit)
                {
                    case StringProcessingUtilities.Edit.None:
                        currentChar = compareString[currentCharIndex] + "";
                        Console.BackgroundColor = ConsoleColor.Black;
                        currentCharIndex++;
                        break;
                    case StringProcessingUtilities.Edit.Replace:
                        currentChar = compareString[currentCharIndex] + "";
                        Console.BackgroundColor = highlightColor;
                        currentCharIndex++;
                        break;
                    case StringProcessingUtilities.Edit.Insert:
                        if (isOriginalString)
                        {
                            Console.BackgroundColor = spaceHighlightColor;
                            currentChar = " ";
                        }
                        else
                        {
                            Console.BackgroundColor = highlightColor;
                            currentChar = compareString[currentCharIndex] + "";
                            currentCharIndex++;
                        }
                        break;
                    case StringProcessingUtilities.Edit.Delete:
                        if (isOriginalString)
                        {
                            Console.BackgroundColor = highlightColor;
                            currentChar = compareString[currentCharIndex] + "";
                            currentCharIndex++;
                        }
                        else
                        {
                            Console.BackgroundColor = spaceHighlightColor;
                            currentChar = " ";
                        }
                        break;
                }

                Console.Write(currentChar);
                Console.BackgroundColor = ConsoleColor.Black;
            }

            // Set the color back to black.
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }
}
